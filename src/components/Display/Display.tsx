/* eslint-disable react/no-unescaped-entities */
/* eslint-disable linebreak-style */
import { useMetaMask } from '~/hooks/useMetaMask'
import { formatChainAsNum, formatAddress } from '~/utils'
import styles from './Display.module.css'

export const Display = () => {

  const { wallet, testTransfer } = useMetaMask()

  return (
    <div className={styles.display}>
      {wallet.accounts.length > 0 &&
        <>
          <div>Wallet Accounts: {wallet.accounts[0]}</div>
          <div>Wallet Balance: {wallet.balance}</div>
          <div>Hex ChainId: {wallet.chainId}</div>
          <div>Numeric ChainId: {formatChainAsNum(wallet.chainId)}</div>
          <p></p>
          <button onClick={testTransfer}>
            Transfer 0.001ETH
          </button>
          <div>From "{formatAddress(wallet.accounts[0])}" to "{formatAddress('0xF53aeC853c4CD0a46dA3B5E5e0BD8697c2f441C1')}"</div>
        </>
      }
    </div>
  )
}